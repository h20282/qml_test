import QtQuick 2.9
import QtQuick.Window 2.2

Window {
    visible: true;
    width: 640;
    height: 480;
    title: qsTr("Hello");

    Column{
        anchors.centerIn: parent
        spacing: 10;

        children:[
            Rectangle{
                width: 480;
                height: 80;
                color: "#564524";
                radius: 10;

                Row{
                    layoutDirection: Qt.RightToLeft;

                    children: [
                        Rectangle {
                            anchors.right: parent;
                            width: 50;
                            height: 30;
                            color: "red";
                            Text{
                                text: qsTr("加入房间");
                            }
                        }
                    ]
                }
            },
            Rectangle{
                width: 480;
                height: 80;
                color: "#564524";
                radius: 10;
            }
        ]
    }
    MouseArea {
        anchors.fill: parent
        onClicked: Qt.quit();
    }
}

